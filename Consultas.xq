(:Lista de nombres de todos los superheroes ordenados por nombre:)
for $b in doc("superheroes")//marvel/superheroe
order by $b//@nombre ascending
return $b//@nombre
(:poderes de Thor:),
for $b in doc("superheroes")//marvel/superheroe
where $b/@nombre="Thor"
return $b/@poderes
(:nombres de superheroes sin amigos:),
for $b in doc("superheroes")//marvel/superheroe
where $b/@amigos="No tiene"
return $b/@nombre
(:superheroes ordenados por nivel:),
for $b in doc("superheroes")//marvel/superheroe
order by $b/@nivel descending
return ($b/@nombre, $b/@nivel)
(:superheroes con nivel superior a 6:),
for $b in doc("superheroes")//marvel/superheroe
order by $b/@nivel descending
where $b/@nivel>6
return ($b/@nombre, $b/@nivel)
